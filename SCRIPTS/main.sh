#! /bin/bash

main() {
    base_install=$1
    popOS_install=$2
    uh="/usr/share"

    if [ $popOS_install -eq 1 ];then
        # on fait les choses bien
        TITREGAMING "${PINK}" "${dir}"
    fi
    

    apps_install

    # TUXEDO APT REPO & CONTROL CENTER
    # install the Tuxedo Control Center in cases of PopOS install (for fan control), or ciphered install (for LUKS password modification GUI)
    lsblk_query="$(lsblk)"
 	if [[ $popOS_install -eq 1 || $lsblk_query == *"crypt"* ]];then
        if ! has_command tuxedo-control-center ;then
            tuxedo_install
         fi
    fi

    # backgrounds, preferences etc. (also deals with PopOS specifics)
    settings_and_schemas

    ## disabled until approved -> see 'util-support' branch
    # install_support_shortcuts


    ### OEM "ship to end user" desktop shortcut -> ./oem_shortcut.sh
    if ! check_oem_shortcut;then
        # PopOS doesn't currently have an OEM install mode, so we'll create it
        answerOEM=""
        DS "\nRaccourci bureau pour activer le mode OEM ? (O/n) " && printf " > " && read answerOEM
        if [ "$answerOEM" != "${answerOEM#[Oo]}" ]; then # "#[]", any O or o in 1st position will be dropped.
            install_oem_shortcut
        fi
    else
        DSBU "Raccourci OEM OK"
    fi
    
}


update() {
    DSBU "\n UPDATE IN PROGRESS. This may take some time..."
    progressSpin & # & for background process
    pS_pid=$!
    echo -e ${blue}'###                       [20%]\r'${rcolor}
    checkCL "apt-get update" 
    checkCL "apt-get upgrade -y"
    # if 'apt-get update' failed with exit code 100 check your internet connection
    # apt-get full-upgrade -y (not required)
    echo -e ${blue}'##############            [60%]\r'${rcolor}
    # Removes old kernels (keep one old and current)
    checkCL "apt-get autoremove -y" # check with : uname -r
    echo -e ${blue}'##########################[100%]\r'${rcolor}
    kill ${pS_pid}
    DSBU "UPDATE COMPLETED."
}


apps_install() {
    ### REGULAR APPS

    # Activate Canonical ppa + repositories update
    DS "Activate Canonical ppa + repositories update"
    checkS "sed -ie \"/# deb http:\/\/archive.canonical.com\/ubuntu\ /s/# //\" /etc/apt/sources.list"

    # Install favorite apps from apps_to_install.list and initial setup of libdvd-pkg
    DS "Install favorite apps from apps_to_install.list && initial setup of libdvd-pkg"
    DSBU "\n INSTALLATION IN PROGRESS. This may take some time..."
    progressSpin & # & for background process, aesthetic only
    pS_pid=$!
    FAVS="$dir/RESOURCES/apps_to_install.list"
    DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends $(cat $FAVS) >/dev/null && checkL
    DEBIAN_FRONTEND=noninteractive apt-get -y remove grub2-themes-ubuntustudio >/dev/null && checkL #removing ugly ubuntustudio grub theme that keeps installing itself
    #checkCL "snap install codium --classic" # if one day you need vs-code alternative

    ### PopOS-GAMING APPS

    if [ $popOS_install -eq 1 ];then
        # adding support for 32bits apps, mandatory for steam install
        sudo dpkg --add-architecture i386
        gFAVS="$dir/RESOURCES/apps_to_install_gaming.list"
        DEBIAN_FRONTEND=noninteractive apt-get update >/dev/null && checkL # need to update here because of the added architecture i386
        DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends $(cat $gFAVS) >/dev/null && checkL
    fi

    kill ${pS_pid} # kills 'progressSpin' (aesthetic only)

    update

    DSBU "INSTALLATION COMPLETE."
    # apt-get --fix-broken install && apt-get update && apt-get autoremove -y # better to use ftrace than fix-broken
}

settings_and_schemas() {
    # Installing Microsoft fonts with .deb (sometimes required, avoid errors) && Editing dock apps
    DS "Installing Microsoft fonts && Editing dock apps"
    checkCL "echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections"
    override="99_launcher.favorites.gschema.override"
    checkCL "cp -r $dir/RESOURCES/${override} ${uh}/glib-2.0/schemas/${override}" "glib-compile-schemas $uh/glib-2.0/schemas/"

    # Changing background
    DS "Changing background"
    Bg="${uh}/backgrounds"
    Go="${uh}/glib-2.0/schemas/10_ubuntu-settings.gschema.override"
    checkCL "cp $dir/IMAGES/backgroundCommown* $Bg" "cp -r $dir/RESOURCES/10_ubuntu-settings.gschema.override $Go"
    # in case of Pop_OS install :
    Go2="${uh}/glib-2.0/schemas/50_pop-desktop.gschema.override"
    Go3="${uh}/glib-2.0/schemas/50_pop-session.gschema.override"
    if [ -f $Go2 ];then
        checkCL "cp -r $dir/RESOURCES/50_pop-desktop.gschema.override $Go2"
    fi
    if [ -f $Go3 ];then
        checkCL "cp -r $dir/RESOURCES/50_pop-session.gschema.override $Go3"
    fi
    checkDF "-f" "$Bg/backgroundCommown.jpg" && checkDF "-f" "$Go" && checkCL "glib-compile-schemas ${uh}/glib-2.0/schemas/"


    if ! check_oem_shortcut;then
        # PopOS doesn't currently have an OEM install mode, so we'll create it
        answerOEM=""
        DS "\nRaccourci bureau pour activer le mode OEM ? (O/n) " && printf " > " && read answerOEM
        if [ "$answerOEM" != "${answerOEM#[Oo]}" ]; then # "#[]", any O or o in 1st position will be dropped.
            install_oem_shortcut
        fi
    else
        DSBU "Raccourci OEM OK"
    fi
    
}


tuxedo_install() {
    DSBU "\n Installing Tuxedo APT repository & tuxedo-control-center"
    # adding Tuxedo's repository
    wget -O - https://deb.tuxedocomputers.com/0x54840598.pub.asc > 0x54840598.pub.asc
    gpg --dearmor 0x54840598.pub.asc
    cat 0x54840598.pub.asc.gpg | sudo tee -a /usr/share/keyrings/tuxedo-keyring.gpg > /dev/null
    if [ -f /etc/apt/sources.list.d/tuxedo-computers.list ];then 
        sudo rm -fv /etc/apt/sources.list.d/tuxedo-computers.list
        # if this isn't done and script is ran multiple times, apt-get update will output warnings due to multiple copies of the same repo line 
    fi 
    echo 'deb [arch=amd64  signed-by=/usr/share/keyrings/tuxedo-keyring.gpg]  https://deb.tuxedocomputers.com/ubuntu jammy main' | sudo tee -a  /etc/apt/sources.list.d/tuxedo-computers.list
    rm 0x54840598* && checkL
    DEBIAN_FRONTEND=noninteractive apt-get update && checkL
    DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends tuxedo-control-center >/dev/null && checkL
}