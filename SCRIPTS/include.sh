#! /bin/bash

# Directory
dir="$(dirname "$0")"

# Shell colors :
CYAN="\\033[1;96m" && PINK="\e[38;5;198m" && RED="\\033[1;31m" && GREEN="\e[038;5;82m" && RESETCOLOR="\\033[0;0m"
underlined=$(tput sgr 0 1) && blue=$(tput setaf 4) && magenta=$(tput setaf 5) && yellow=$(tput setaf 3) && tCyan=$(tput setaf 6) && rcolor=$(tput sgr0) # reset color

# FUNCTIONS :
couleurs=(Noir Rouge Vert Jaune Bleu Magenta Cyan Blanc)
colors() {
    for i in $(seq 1 7); do
        echo -e " $(tput setaf $i)${couleurs[$i]}\tTexte$(tput sgr0)\t$(tput bold)$(tput setaf $i)Texte$(tput sgr0)\t$(tput sgr 0 1)$(tput setaf $i)Texte$(tput sgr0)\t(tput setaf $i)"
    done
}
# uncomment to display all possibilites (colors, underlined, bold) :
#colors

# USE : countdown <seconds> (countdown 5)
countdown() {
    start="$(($(date '+%s') + $1))"
    while [ $start -ge $(date +%s) ]; do
        time="$(($start - $(date +%s)))"
        printf "%s\r" "${yellow}$(date -u -d "@$time" +%M:%S)${rcolor}" #%H:%M:%S
        sleep 0.1
    done
}

# USE : fixed_length <length> <string>
# prints the string, and adds spaces to match the target fixed length
fixed_length() {
	length=$1
	string=$2
	while [[ ${#string} -lt $length ]];do
		string="$string."
	done
	
	printf "$string"
}

# check if script is run as root and internet connection is available and systemd
checkInitInternet() {
    
    # Checking the internet connection
    DS "Checking the internet connection" && checkCL "wget -q --spider https://commown.coop/"
    # Wait for the systemd units
    autoUpdate "0" "45"
}

checkInitAdmin () {
    #Check if root # [[ $EUID -ne 0 ]] && echo "This script must be run as root." && exit 1
    # Verification des permissions root
    errmsg="Ce script a besoin des permissions administrateur (sudo), veuillez le lancer de la manière suivante : sudo ./install/commownScript.sh"
    if [ $(whoami) != 'root' ]; then
        echo -e ${RED}"$errmsg"${RESETCOLOR} && exit 0
    fi
}

# check if file or directory exists
checkDF() {
    if [ "$1" "$2" ]; then # -f file/ -d directory
        # :                  # don't display [OK] (The no-op command in shell is : (colon).)
        echo -e ${tCyan}"\e[0K\r$2 exists."${GREEN}"    [OK]"${RESETCOLOR}
    else
        echo -e ${tCyan}"\e[0K\r$2 doesn't exist, line : ${BASH_LINENO[0]}"${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
    fi
}

############################################################## CHECK ###########################################################################
# checkCL, checkS, checkL : Those 3 functions can be improved, maybe with a function that can regroup all these functions more optimally.

# check if command line is successful
# (USE : checkCL "command1" "command2")
checkCL() {
    for i in "$@"; do
        if $i >/dev/null; then # display $1 output if command is not successful (Hide If No Error)
            # :                  # don't display [OK] (The no-op command in shell is : (colon).)
            echo -e ${tCyan}"\e[0K\r'$i' completed."${GREEN}"    [OK]"${RESETCOLOR}
        else
            echo -e ${tCyan}"\e[0K\r'$i' failed with exit code "$?" at line : ${BASH_LINENO[0]}"${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
        fi
    done
    # Note that A && B || C is not if-then-else. C may run when A is true.
}

# check special (when special characters and checkCL doesn't work, example : sed, perl, etc.)
# (USE : checks "command1") [1 command max]
checkS() {
    if eval "$*" >/dev/null; then # display $1 output if command is not successful (Hide If No Error)
        # :                  # don't display [OK] (The no-op command in shell is : (colon).)
        echo -e ${tCyan}"\e[0K\r'$*' completed."${GREEN}"    [OK]"${RESETCOLOR}
    else
        echo -e ${tCyan}"\e[0K\r'$*' failed with exit code "$?" at line : ${BASH_LINENO[0]}"${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
    fi
}

# check exit status of the Last executed command (if checkS doesn't work, special regex)
checkL() {
    if [ $? -eq 0 ]; then
        echo -e ${tCyan}"\e[0K\rLine ${BASH_LINENO[0]} completed."${GREEN}"    [OK]"${RESETCOLOR}
    else
        echo -e ${tCyan}"\e[0K\rLine ${BASH_LINENO[0]} failed with exit code "$?"."${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
    fi
    # check if command line is successful : $? = 0 if successful ; $? = 1 if not successful
}

#################################################################################################################################################

# CRITICAL FUNCTION : (disable or enable updates -> disable : might cause some security problems for some users)
# PREVENT concurrent access : "could not get lock /var/lib/dpkg/lock-frontend"
# USE : autoUpdate "0" "60" (case 0 60 seconds, 0 is the only one with second argument), autoUpdate "1", autoUpdate "2", etc.
autoUpdate() {
    case ${1} in
    "0") # SAFEST alternative, strongly advise to use this one:
        DSBU "Wait for the systemd units, this could take a while..."
        countdown ${2} & # & for background process
        pS_pid=$!
        warning="Timeout reached at line : ${BASH_LINENO[0]}, please restart the script or wait for systemd units to end."
        # & for background process
        timeout ${2} systemd-run --property="After=apt-daily.service apt-daily-upgrade.service" --wait --quiet /bin/true ||
            (
                [[ $? -eq 124 ]] && kill ${pS_pid} && echo -e ${RED}"\e[0K\rWARNING: "${tCyan}"${warning}"${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
            )
        kill ${pS_pid} # kill the background process
        # Cf. https://unix.stackexchange.com/questions/463498/terminate-and-disable-remove-unattended-upgrade-before-command-returns/463503#463503
        # (124=cmd timed out, 127=cmd not found, 137=cmd killed) USEFUL OPTIONS timeout : "--preserve-status" "--v"
        msg=${tCyan}"\e[0K\rThe systemd units are done."${GREEN}"    [OK]"${RESETCOLOR}
        ;;
    "1") # Alternative 1 (uninstalled) :
        checkCL "apt-get remove unattended-upgrades -y"
        msg=${tCyan}"\e[0K\rUnattended-upgrades uninstalled."${GREEN}"    [OK]"${RESETCOLOR}
        ;;
    "2") # Alternative 1  (installed):
        checkCL "apt-get install unattended-upgrades"
        msg=${tCyan}"\e[0K\rUnattended-upgrades installed."${GREEN}"    [OK]"${RESETCOLOR}
        ;;
    "3") # Alternative 2 (disabled) :
        regex="s/(?<=\")(.*?)(?=\";)/0/gm"
        perl -pi -e "${regex}" /etc/apt/apt.conf.d/20auto-upgrades >/dev/null && checkL # checkS not valid with special regex
        msg=${tCyan}"\e[0K\rAuto update disabled."${GREEN}"    [OK]"${RESETCOLOR}
        ;;
    "4") # Alternative 2 (enabled) :
        regex="s/(?<=\")(.*?)(?=\";)/1/gm"
        perl -pi -e "${regex}" /etc/apt/apt.conf.d/20auto-upgrades >/dev/null && checkL # checkS not valid with special regex
        msg=${tCyan}"\e[0K\rAuto update enabled."${GREEN}"    [OK]"${RESETCOLOR}
        ;;
    "5") # Check unclean state :
        msg=${tCyan}"\e[0K\rChecking for updates..."${GREEN}"    [OK]"${RESETCOLOR}
        dpkg --configure -a # Make sure any packages in an unclean state are installed correctly
        ;;
    *) # Invalid X argument on 'autoUpdate "X"':
        msg=${tCyan}"\e[0K\rInvalid argument."${RED}"    [NOT OK]"${RESETCOLOR} && exit 1
        ;;
    esac

    echo -e ${msg}

    # WITH PROMPT : sudo dpkg-reconfigure -plow unattended-upgrades
    # WITHOUT PROMPT : sudo gedit /etc/apt/apt.conf.d/20auto-upgrades

    # ps aux | grep -i apt && kill <process_id> (If waiting for cache lock)
}

# Display Sring with colors
DSP() { echo -e ${PINK}"$1"${RESETCOLOR}; }
DS() { echo -e ${CYAN}"$1"${RESETCOLOR}; }
DSM() { echo -e ${magenta}"$1"${rcolor}; }
DSU() { echo -e ${blue}"$1"${rcolor}; }
DSMU() { echo -e ${underlined}${magenta}"$1"${rcolor}; }
DSBU() { echo -e ${underlined}${blue}"$1"${rcolor}; }

# Title Commown
TC() { echo -e $1"" && cat $2/RESOURCES/commown_text.txt && echo -e ""${RESETCOLOR} && sleep 1; }
# Title Autoextend
TITREAUTOEXTENSION() { echo -e $1"" && cat $2/RESOURCES/autoextension_text.txt && echo -e ""${RESETCOLOR} && sleep 1; }
# Title Gaming
TITREGAMING() { 
    terminal_width=$(tput cols)
    if [ $terminal_width -lt 116 ];then 
        echo -e $1"" && cat $2/RESOURCES/gaming_text_small.txt && echo -e ""${RESETCOLOR} && sleep 1
    else
        echo -e $1"" && cat $2/RESOURCES/gaming_text.txt && echo -e ""${RESETCOLOR} && sleep 1
    fi
}

# USE : progressSpin & # & for background process
progressSpin() {
    while :; do for s in / - \\ \|; do
        echo -ne ${GREEN}"\e[0K\r$s"${rcolor} && sleep .1
    done; done
}

getDisk() { # not used
    # get all disks into an array sd and nvme
    sd=($(lsblk -d | grep -E 'sd[a-z]' | awk '{print $1}'))
    nvme=($(lsblk -d | grep -E 'nvme[0-9]' | awk '{print $1}'))
    disks=(${sd[@]} ${nvme[@]})

    # Only use to test, comment later, only echo $i below (it is like a return value)
    # DS "Disks with and without partitions :"
    # for disk in ${disks[@]}; do
    #     DS "$disk "
    # done

    # loop over disks in array
    for i in $(echo ${disks[@]}); do
        # select only thos without partitions
        # check if it has a partition
        diskcheck=$(ls /dev/${i}* | grep '[0-9]')
        # if not, print it
        if [ -z "$diskcheck" ]; then
            echo $i
        fi
    done
}

reboot_oem () {

# Reboot prompt
    TC "${GREEN}" "${dir}" # Title Commown
    echo -e ${GREEN}"TERMINÉ !"${RESETCOLOR}


    ### TODO (bonus): 
    # find out if user is OEM, else he cannot launch oem-config-prepare

    # problem with these attributions : if sudo launches 'id', it returns only 0, even if oem exists
    # if 'id -u oem' is launched but oem doesn't exist, the command returns an error and the script stops there
    #	oemID=($(id -u oem))
    #	looking_for_oemid+=($(id | grep '29999')) # <-- launched as sudo : returns only his info, not oem's
    #	looking_for_oem+=($(id | grep 'oem'))

    #if [[ -n $looking_for_oem && -n $looking_for_oemid ]];then
    # if user is not oem, the 'oem-config-prepare' command doesn't exist
    # a plain 'id -u' would return 0 (root user)

    if has_command oem-config-prepare;then

        answerOR="" # OEM & reboot
        DS "Désirez vous redémarrer immédiatement avec OEM (o)? / Ou le faire manuellement (n)? " && read answerOR
        if [ "$answerOR" != "${answerOR#[Oo]}" ]; then
            DSBU "Redémarrage en cours, launcher OEM actif. \nRedémarrage dans : "
            # Timer 15s, delete install folder , launch oem, reboot :
            countdown 15
            
            if [[ "$PWD" != / ]]; then # if not in root, you are in session before launching oem
                #checkS "gtk-launch oem-config-prepare-gtk.desktop"
                checkS "oem-config-prepare"
            fi
            if [[ "$PWD" != /media* ]]; then # we want to delete this script only if it's stored on the customer's machine
                checkCL "rm -rf ${dir}" "reboot now"
            fi
        else # if not y, you want to reboot manually
            DSBU "Veuillez lancer le launcher oem 'oem-config-prepare'. \nPuis redémarrez votre ordinateur.\n"
        fi

    else 
    	DS "Utilisateur OEM non-détecté, fin du script.\n"
    fi

}

has_command() {
  command -v $1 > /dev/null
}
