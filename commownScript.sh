#! /bin/bash

set -euo pipefail # throwErrors

source ./install/SCRIPTS/include.sh 			    	# functions for colors, display, checks, etc.
source ./install/SCRIPTS/autoextend_diskspace.sh  		# extend root part/volume into unallocated space
source ./install/SCRIPTS/main.sh    			    	# installs apps, settings, backgrounds...
source ./install/SCRIPTS/plymouth_install.sh  		    # Plymouth theme for boot and LUKS unlock screens
source ./install/Multiboot/install_multiboot.bash		# GRUB bootmanager GUI installer 
source ./install/SCRIPTS/luks_clone.sh	            	# to run after cloning a LUKS image : reencrypt & extend on remaining disk space
source ./install/SCRIPTS/oem_shortcut.sh                # OEM desktop shortcut installation for PopOS : OEM mode not included in installer

#########################################################

# CODE STRUCTURE

    # if : no options provided
        # - bienvenue && menu
    # if : options provided
        # - hard code menu's answers according to options
        # - bienvenue && en_route

# bienvenue :   initial checks & update, visual presentation
# menu :        user will be prompted for the options they want -> launches 'en_route' if user's answers are correct
# en_route :    actual script : calls function to execute appropriate tasks


#TODO
    # - update la version des repo à la prochaine version d'Ubuntu (jammy --> ???)

    # - ajuster dans les schémas le bon paramétrage pour "bouton Power" -> "suspendre" (et non "éteindre")
    # - menu
    #   - affiner le grain des choix, par ex: permettre la MàJ des schémas seulement, ou du repo Tuxedo....
    #   - changer l'interface ? permettre par exemple de faire tous les choix d'un coup en affichant direct l'ensemble des possibilité, comme une checklist ?
    # - progressSpin semble ne pas se désactiver parfois
    # - OEM : vérification du raccourci, ne pas l'installer si il est déjà opé
    # - autoextend :
    #   - détection d'espace libre pour cacher ou non le choix 'autoextend'
    #   - ne pas proposer d'étendre en dessous de 2Mo libres
    #   - passer de parted à fdisk ? si cela permet plus de flexibilité, notamment pour la gestion de plusieurs OS
    # - ajouter fonds d'écran commown dans la sélection par défaut, cf. https://askubuntu.com/questions/66208/how-to-modify-the-system-default-background-image
    # - ajuster les options du schéma (RESOURCES/10_ubuntu...) afin de ne plus recevoir d'erreur (erreurs en blanc lors de l'installation : 'cette clé n'existe pas dans le schéma...')
    # - nettoyer le code en retirant les vieilles options commmentées et scripts non-utilisés
    # - uniformiser l'utilisation des 'check' : checkCL serait à privilégier car montre la commande dans le récap
    # - intégrer pleinement Multiboot dans la structure du reste du projet ?
    # - changer Desktop->Bureau dans ~/.config/user-dirs.dirs, et mv Desktop/ Bureau/
    #   - puis reboot (?) à tester : parfois les dossiers home se retrouvent juste déplacés sur le bureau
    #   - après reboot la ligne dans le fichier de config qui devrait afficher =Bureau affiche juste =<rien>
    #     -> il faut la changer à nouveau et reboot
    #     -> à automatiser si on en a le temps

#TODO (spécifique gaming): 

    # - oem_shortcut : ne pas autoriser la création d'oem si l'utilisateur n'est pas 'oem', ou bien trouver le moyen de le changer pour de vrai (la tentative via GUI ne changeait pas l'output de 'id')
    # - bouton d'agrandissement des fenêtres (à activer par GUI dans Tweaks -> trouver moyen en CLI) 
    # - installer vulkan 32 bits pour améliorer la compatibilité de Lutris (pas moyen de trouver le package, caché dans apt pour notre installation)
    # - GRUB : non-installé par défaut sous PopOS : option à ajouter (cf. "Installation de GRUB manuelle" : manip dans doc dédié : https://nuage.commown.coop/f/139058)


#########################################################




# Init variables
vExtend_unalloc=0 && vMain=0 && vGrub=0 && vPlymouth=0 && vGaming=0 && vRecipher=0
answer1="" && answer2="" && answer3="" && answer4="" && answer5="" # text because user-filled, true={'O'*;'o'*}
optUbuntu=0 && optGaming=0 && optCipher=0 && optExtend=0 && optHelp=0 && optInvalid=0

# Options management (if no option is selected, the user will go through the menu)
while getopts ":ugceh" option; do
    case $option in
        u) # basic ubuntu install
            optUbuntu=1;;
        g) # gaming PopOS install
            optGaming=1;;
        c) # re-cipher & extend ciphered install
            optCipher=1;;
        e) # autoextend into unallocated disk space
            optExtend=1;;
        h) # display Help
            optHelp=1;;
        \?) # Invalid option
            optHelp=1
            optInvalid=1;;
    esac
done



aide()
{
    # Display Help
    echo
    DS "Commown Install - script d'installation Ubuntu / PopOS"
    echo " < version $(cat ./install/version.txt) >"
    echo
    echo
    echo "Syntaxe : sudo bash ./install/commownScript.sh -[u|g|c|e|h]"
    echo
    echo "Ajouter une (seule) option pour automatiser les choix"
    echo "-> Options :"
    echo "  u   Installation 'Ubuntu classique' : main, thème GRUB, Plymouth"
    echo "  g   Installation 'PopOS gaming' : main, Plymouth, apps gaming"
    echo "  c   Re-chiffrement & extension d'une installation chiffrée LVM-LUKS"
    echo "  e   Auto-extension de la partition root dans espace non-alloué adjacent"
    echo "  h   Afficher ce message"
    echo
}

bienvenue () {

    printf '\033[8;50;200t' # resize terminal
    TC "${CYAN}" "${dir}"   # Title Commown
    echo " < version $(cat ./install/version.txt) >"
    echo
    checkInitAdmin
    checkInitInternet

    update # call update function in main.sh : update (kernel, apps)
}

menu () {

    printf "\nVoulez vous ..."
    fixed_length 80 "\n - Procéder aux installations de base ? (update, thèmes, apps, etc.) " && printf " (1)"
    fixed_length 80 "\n - Re-chiffrer et étendre une installation LUKS clonée ? " && printf " (2)"

    DS "\n\nEntrez votre choix : " && read answerRecipher
    
    
    # Checking if we will propose Pop_OS gaming installation options
    popOS=0
    popOScheck="$(cat /etc/os-release)"
    if [[ $popOScheck == *"Pop!_OS"* ]];then popOS=1;fi
    #TODO : add free disk space check

    if [[ $answerRecipher == '1' ]];then # here we "fork" between the regular usage (installations) and the exceptional standalone re-cipher function

        # Prompt User
            # the way of testing (x == 0 ?) is inherited from the old answer management :
            # answer could be hard coded as =2 by the script into itself to avoid task repetition from a launch to another :
            # that was messy
        # fixed_length function in include.sh for nice column presentation
        
        if [ $vExtend_unalloc -eq 0 ]; then
            fixed_length 55 "Extension du disque dans espace non-alloué (o/n)? " && read answer1 
            if [ "$answer1" != "${answer1#[Oo]}" ]; then # "#[]", any O or o in 1st position will be removed.
                vExtend_unalloc=1   
            fi
        fi

        if [ $vMain -eq 0 ]; then
            fixed_length 55 "Main - installations Ubuntu Commown (o/n)? " && read answer2
            if [ "$answer2" != "${answer2#[Oo]}" ]; then # "#[]", any O or o in 1st position will be removed.
                vMain=1
            fi
        fi

        if [ $vGrub -eq 0 ]; then
            fixed_length 55 "Thème GRUB (o/n)? " && read answer3
            if [ "$answer3" != "${answer3#[Oo]}" ]; then # "#[]", any O or o in 1st position will be removed.
                vGrub=1
            fi
        fi

        if [ $vPlymouth -eq 0 ]; then
            fixed_length 55 "Thème Plymouth (o/n)? " && read answer4
            if [ "$answer4" != "${answer4#[Oo]}" ]; then # "#[]", any O or o in 1st position will be removed.
                vPlymouth=1
            fi
        fi

        if [[ $vGaming -eq 0  && $popOS -eq 1 ]]; then
            fixed_length 55 "[Pop_OS] Installations Gaming (o/n)? " && read answer5
            if [ "$answer5" != "${answer5#[Oo]}" ]; then # "#[]", any O or o in 1st position will be removed.
                vGaming=1
            fi
        fi

        en_route

    elif [[ $answerRecipher == '2' ]];then

        vRecipher=1
        en_route 

    else

        printf "\nRéponse invalide, sortie du script."

    fi
}


en_route () {

    checkCL "cp -f ./install/version.txt /etc/commown-install-version"
    checkCL "chmod 755 /etc/commown-install-version"

    if [ $vExtend_unalloc -eq 1 ]; then
        sleep 1 && DSBU "\nEXTEND into unalloc START" && extend_unalloc && DSBU "EXTEND unalloc END \n" 
        # call extend_unalloc function in autoextend_diskspace.sh
    fi
    if [[ $vMain -eq 1 || $vGaming -eq 1 ]]; then
        sleep 1 && DSBU "\nMAIN START" && main $vMain $vGaming && DSBU "MAIN END \n"     
        # call main function in main.sh
        # -> supply two arguments to customize the installation
    fi
    if [ $vGrub -eq 1 ]; then
        sleep 1 && DSBU "\nGRUB THEME START" && multiboot_install && DSBU "GRUB THEME END \n"    
        # call multiboot_install in Multiboot/install_multiboot.sh
    fi
    if [ $vPlymouth -eq 1 ]; then
        sleep 1 && DSBU "\nPLYMOUTH THEME START" && plymouth_install && DSBU "PLYMOUTH THEME END \n"     
        # call plymouth_install function in plymouth_install.sh
    fi
    if [ $vRecipher -eq 1 ]; then
        sleep 1 && DSBU "\nRE-CIPHER & EXTEND START" && luks_clone_setup && DSBU "RE-CIPHER & EXTEND END \n"  
        # call luks_clone_setup function in luks_clone.sh
    fi

    # Removing horrible ubuntustudio grub theme that somehow installs itself and get priority over others
    if [[ -d /boot/grub/themes/ubuntustudio ]];then 
		sudo rm -rf /boot/grub/themes/ubuntustudio
        sudo update-grub
	fi
    DEBIAN_FRONTEND=noninteractive apt-get -y remove grub2-themes-ubuntustudio >/dev/null && checkL


    echo ""
    DSP "Certains changements peuvent ne pas faire effet avant redémarrage (fond d'écran, activation du raccourci OEM ... )"
    echo ""

    # reboot_oem # may do more harm than good

}



if [ $optInvalid -eq 1 ];then

    echo -e ${RED}"Erreur : option invalide"${RESETCOLOR}

elif [ $optHelp -eq 0 ];then

    if [ $optUbuntu -eq 1 ];then
        vMain=1 && vGrub=1 && vPlymouth=1
        DSP "Option : Installation 'Ubuntu classique' : main, thème GRUB, Plymouth"
        bienvenue
        en_route
    elif [ $optGaming -eq 1 ];then
        vMain=1 && vPlymouth=1 && vGaming=1
        DSP "Option : Installation 'PopOS gaming' : main, Plymouth, apps gaming"
        bienvenue
        en_route
    elif [ $optCipher -eq 1 ];then
        vMain=1 && vGrub=1 && vPlymouth=1
        DSP "Option : Re-chiffrement & extension d'une installation chiffrée LVM-LUKS"
        bienvenue
        en_route
    elif [ $optExtend -eq 1 ];then
        vExtend_unalloc=1
        DSP "Option : Auto-extension de la partition root dans espace non-alloué adjacent"
        bienvenue
        en_route
    else
        #no options provided
        bienvenue && menu
    fi

fi

if [ $optHelp -eq 1 ];then

    aide

fi 











